# git
git init
git remote add origin git@gitlab.com:akhmetovai/<proj-name>.git
git add --all
git commit -m "version 0.1.0"
git push origin master

# Проверка докера локально:
sudo docker build -t local_hat_bot -f Dockerfile .
sudo docker run -p 5000:5000 -e PORT=5000 local_hat_bot

# Деплой на heroku:
heroku login
heroku git:remote -a hat-bot-dmia
heroku buildpacks:set heroku/python
heroku stack:set container
git push heroku master
heroku open

# Для дебага:
heroku run rails console
heroku restart