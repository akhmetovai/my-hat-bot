import json
import os
import random

import fasttext
from flask import Flask, render_template, request

from player import LocalFasttextPlayer

app = Flask(__name__)


# show funny cat gifs
@app.route("/")
def index():
    return render_template("index.html")


player = LocalFasttextPlayer(model=fasttext.load_model("models/skipgram.model"))


@app.route("/explain")
def explain():
    word = request.args.get("word")
    n_words = int(request.args.get("n_words"))
    return json.dumps(player.explain(word=word, n_words=n_words))


@app.route("/guess")
def guess():
    words = request.args.getlist("words")
    n_words = int(request.args.get("n_words"))
    return json.dumps(player.guess(words=words, n_words=n_words))


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=int(os.environ.get("PORT", 5000)))
