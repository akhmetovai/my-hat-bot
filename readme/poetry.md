# Затестим Poetry

# Документация:
https://python-poetry.org/docs/

$ curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
$ source $HOME/.poetry/env
$ poetry --version

# Для удобства, можно добавить автодополнение команд в poetry:
$ poetry completions bash > /etc/bash_completion.d/poetry.bash-completion
# Если не работает:
$ poetry completions bash >> ~/.bash_completion

# Начало работы. Переходим в директорию проекта и выполняем команды:
$ poetry init

# Инициализируем виртуальное окружение
$ poetry env use python3.9
$ poetry shell

# Запуск команд без захода в виртуальное окружение:
$ poetry run python

# Добавление библиотек в виртуальное окружение:
$ poetry add requests

# Показать текущее дерево завимостей:
$ poetry show --tree

# Проверка доступности более свежих обновлений по установленным пакетам:
$ poetry show --latest

# Запуск команд с помощью poetry может быть выполнена с добавлением следующих строк в pyproject.toml:
[tool.poetry]
# ...
packages = [
    { include = "mypackage" },
]

[tool.poetry.scripts]
mycommand = "mypackage.print_hello:run"

# Соответствующий вызов команды
$ poetry run mycommand

# make requirements.txt
$ poetry export -f requirements.txt --output requirements.txt