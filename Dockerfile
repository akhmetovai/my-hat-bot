FROM python:3-buster
ARG PROJNAME=hat-player
ENV PROJNAME=${PROJNAME}
RUN mkdir /${PROJNAME}
WORKDIR /${PROJNAME}

# python packages
COPY requirements.txt .
RUN pip install --use-feature=2020-resolver --no-dependencies --no-cache-dir -r requirements.txt

# finally
COPY . .

# set env vars
# heroku will supply PORT value, and you should supply it yourself when running
ENV FLASK_RUN_PORT=$PORT

# run the command
RUN useradd -m myuser
USER myuser
CMD ["python", "flask-app.py"]