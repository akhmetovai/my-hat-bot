# Подробная документация:
https://dvc.org/doc

# Установка (Можно опционально задать конкретный тип файловой системы [s3], [azure], [gdrive], [gs], [oss], [ssh])
poetry add dvc

# Начало работы:
poetry shell
dvc init

# Коммитим в гит новые файлы .dvcignore + .dvc/*
git add --all
git commit -m "version 0.2.0"
git push origin master


-----------------------------------------

# Info. Пример добавления данных
dvc add data/data.json
git add data/data.xml.dvc data/.gitignore
git commit -m "Add raw data"

# Info. Пример подключения облачного хранилища
dvc remote add -d storage s3://your-bucket/your-storage
git commit .dvc/config -m "Configure remote storage"

# Info. Отправление данных в хранилище
dvc push

# Info. Получение данных из хранилища
dvc pull

# Info. Добавление данных
dvc add data/data.json
git commit data/data.xml.dvc -m "Dataset updates"
dvc push

# Info. Переключение между версиями
git checkout <...>
dvc checkout

# Info.Есть Python API
import dvc.api

with dvc.api.open(
        'get-started/data.xml',
        repo='https://github.com/interative/dataset-registry'
    ) as fd:
    # fd - можно работать как с обычным файлом

# Есть базовые возможности работы с data pipelines а-ля микроDAG
# Но обычно для этого используется airflow

-------------------------------------------


# Для примера, создадим проект в Google Cloud (регаемся, привязываем карту, не забыв в дальнейшем отвязать)
https://console.cloud.google.com/home

# Google cloud storage - создаем бакет для хранения
https://console.cloud.google.com/storage/browser

# Во вкладке configuration находим адрес бакета
gs://hat-bot-dmia

# К бакету мы еще вернемся, а пока добавим наши данные в dvc
dvc add texts/*
git add texts/20_newsgroups.dvc
git commit -m "version 0.2.1"

# Теперь настало время вернуться к Google Cloud
# Нам нужно установить необходимые библиотеки для работы с GCP CLI
poetry add google-cloud-storage
gcloud init

dvc remote add -d myremote gs://hat-bot-dmia
git commit .dvc/config -m "version 0.2.3"

poetry add "dvc[gs]"
dvc push

# Ругается, что анонимус пытается записать данные. Нужно авторизоваться
